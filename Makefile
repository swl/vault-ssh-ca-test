USER=swl
ROLE=clientrole
VAULT_ADDR=http://127.0.0.1:8200
VAULT_TOKEN=root

server:
	vault server -dev -dev-root-token-id=${VAULT_TOKEN} -log-level=debug -log-format=standard &

status:
	vault status


client-ca.pub:
	ssh-keygen -t rsa -N '' -C CLIENT-CA -b 4096 -f client-ca

setup: client-ca.pub
	vault policy write ssh-user user-policy.hcl
	vault auth enable userpass
	vault write auth/userpass/users/swl password=secret policies=ssh-user
	vault write auth/userpass/users/scotty password=topsecret policies=ssh-user
	vault secrets enable -path=ssh-client-signer ssh
	jq --arg accessor `vault auth list -format=json | jq -r '.["userpass/"].accessor'` '.allowed_users = "{{identity.entity.aliases."+$$accessor+".name}}"| .default_user = "{{identity.entity.aliases."+$$accessor+".name}}"' ssh-signer-role.hcl.tmpl > ssh-signer-role.hcl
	vault write ssh-client-signer/config/ca private_key=@client-ca public_key=@client-ca.pub
	vault write ssh-client-signer/roles/$(ROLE) @ssh-signer-role.hcl

scotty:	scotty-sign.hcl
	vault login -method=userpass username=scotty password=topsecret
	vault write -field=signed_key ssh-client-signer/sign/$(ROLE) public_key=@$(HOME)/.ssh/id_rsa.pub @scotty-sign.hcl > $(HOME)/.ssh/id_rsa-cert.pub
	ssh-keygen -L -f $(HOME)/.ssh/id_rsa-cert.pub

scotty2: scotty2-sign.hcl
	vault login -method=userpass username=scotty password=topsecret
	vault write -field=signed_key ssh-client-signer/sign/$(ROLE) public_key=@$(HOME)/.ssh/id_rsa.pub @scotty2-sign.hcl > $(HOME)/.ssh/id_rsa-cert.pub
	ssh-keygen -L -f $(HOME)/.ssh/id_rsa-cert.pub

swl:	swl-sign.hcl
	vault login -method=userpass username=swl password=secret
	vault write -field=signed_key ssh-client-signer/sign/$(ROLE) public_key=@$(HOME)/.ssh/id_rsa.pub @swl-sign.hcl > $(HOME)/.ssh/id_rsa-cert.pub
	ssh-keygen -L -f $(HOME)/.ssh/id_rsa-cert.pub

kill:
	killall vault

