# Testing Vault SSH CA

##  Starting Vault

To start a local `vault` server in development mode, run

```shell
make server
```

##  Configuring Vault

Vault needs to be configured with user authentication, SSH CA key pair, and an SSH signing role:

```shell
make setup
```

The important part of the configuration are the `allowed_users` and `default_user` settings (Look in `ssh-signer-role.hcl.tmpl` and `ssh-signer-role.hcl` (which is created from `ssh-signer-role.hcl.tmpl` by the Makefile).

```json
{
  ...
  "allowed_users": "{{identity.entity.aliases.auth_userpass_f9ae3519.name}}",
  "default_user": "{{identity.entity.aliases.auth_userpass_f9ae3519.name}}"
  ...
}
```

In development mode, the `auth_userpass_...` value will change after each `vault` restart. The Makefile automatically updates `ssh-signed-role.hcl` with the correct value.

The auth "accessors" can be listed with

```shell
% vault auth list -format=json|jq -r '.[]|.accessor'
auth_approle_12345678
auth_ldap_23456789
auth_token_3456789a
auth_userpass_456789ab
```

##  Sign an SSH Certificate

The Makefile has targets for signing `~/.ssh/id_rsa.pub` in a variety of ways.

First, we authenticate to vault as `swl`, then request a signed SSH certificate with the `swl` principal. (Look in `swl-sign.hcl`)

```shell
make swl
```

###   Sign another SSH Certificate

Next, we authenticate to vault as `scotty`, then request a signed SSH certificate with the `scotty` principal. (Look in `scotty-sign.hcl`)

```shell
make scotty
```

### Fail to Sign an Invalid Request

Finally, we authenticate to vault as `scotty`, but request a signed SSH certificate with the `swl` principal, which will FAIL because the authenticated username does not match the requested principal. (Look in `scotty2-sign.hcl`)

```shell
make scotty2
```

### Manually Signing a Certificate

If the default extensions used by the SSH CA are sufficient, signing an certificate with vault is straightforward:

```shell
% vault write -field=signed_key ssh-client-signer/sign/clientrole public_key=@$HOME/.ssh/id_rsa.pub valid_principals=swl > ~/.ssh/id_rsa-cert.pub
% ssh-keygen -L -f ~/.ssh/id_rsa-cert.pub
/Users/swl/.ssh/id_rsa-cert.pub:
        Type: ssh-rsa-cert-v01@openssh.com user certificate
        Public key: RSA-CERT SHA256:p85QXAS+amqJ8ymxdK3AVIzKrDx/7xpT4Qpw6wK+Ucc
        Signing CA: RSA SHA256:q9DOPSOHPZZNQK8ZEd6t8rMx9G6TD3sYlje5mrUUfrU (using rsa-sha2-256)
        Key ID: "vault-userpass-swl-a7ce505c04be6a6a89f329b174adc0548ccaac3c7fef1a53e10a70eb02be51c7"
        Serial: 253032986598881463
        Valid: from 2024-05-15T15:37:33 to 2024-05-15T16:08:03
        Principals:
                swl
        Critical Options: (none)
        Extensions:
                permit-pty
```

If additional extensions are required, a config file is required:

```shell
% cat test.hcl
{
  "valid_principals": "swl",
  "extensions": {
    "permit-pty": "",
    "permit-port-forwarding": "",
    "permit-agent-forwarding": "",
  }
}
% vault write -field=signed_key ssh-client-signer/sign/clientrole public_key=@$HOME/.ssh/id_rsa.pub @test.hcl > ~/.ssh/id_rsa-cert.pub
% ssh-keygen -L -f ~/.ssh/id_rsa-cert.pub
/Users/swl/.ssh/id_rsa-cert.pub:
        Type: ssh-rsa-cert-v01@openssh.com user certificate
        Public key: RSA-CERT SHA256:p85QXAS+amqJ8ymxdK3AVIzKrDx/7xpT4Qpw6wK+Ucc
        Signing CA: RSA SHA256:q9DOPSOHPZZNQK8ZEd6t8rMx9G6TD3sYlje5mrUUfrU (using rsa-sha2-256)
        Key ID: "vault-userpass-swl-a7ce505c04be6a6a89f329b174adc0548ccaac3c7fef1a53e10a70eb02be51c7"
        Serial: 11106102282315421402
        Valid: from 2024-05-15T15:40:39 to 2024-05-15T16:11:09
        Principals:
                swl
        Critical Options: (none)
        Extensions:
                permit-agent-forwarding
                permit-port-forwarding
                permit-pty
```

## Cleanup

Run `make kill` to kill the vault server.


# Skipping Duo with Signed SSH Certificates

Most Stanford servers use Duo MFA for SSH logins, which breaks automations.

Since our Vault service requires Duo MFA, it's possible to skip Duo on SSH access to servers if a signed SSH certificate is used.

## Configire Duo for MFA

Follow Stanford's [instructions](https://uit.stanford.edu/service/authentication/twostep/duo_ssl)

After setting up Duo, replace `/etc/pam.d/sshd` with

```
auth    required                    pam_duo.so conf=/etc/security/pam_duo.conf

account    required     pam_nologin.so

...
```

## Test

Ensure that everything is working, and that you're prompted for Duo when you `ssh` to the server.

## Trusting Signed SSH Certificates

The SSH daemon (server) will reject signed certificates unless the CA signing the certificate is trusted (the public key must be in a file referenced from `/etc/ssh/sshd_config` using the `TrustedUserCAKeys` option). Signed certificates will also be rejected if the signature has expired.

```shell
% ssh-keygen -L -f ~/.ssh/id_rsa-cert.pub
/Users/swl/.ssh/id_rsa-cert.pub:
        Type: ssh-rsa-cert-v01@openssh.com user certificate
        Public key: RSA-CERT SHA256:...
        Signing CA: RSA SHA256:... (using rsa-sha2-256)
        Key ID: "vault-userpass-swl-..."
        Serial: 16950714389638918738
        Valid: from 2024-05-15T14:40:01 to 2024-05-15T15:10:31
        Principals:
                swl
        Critical Options: (none)
        Extensions:
                permit-X11-forwarding
                permit-agent-forwarding
                permit-port-forwarding
                permit-pty
```

The principal in the signed certificate must also match a user on the server, or be listed in a file pointed to by the `AuthorizedPrincipalsFile` option in `/etc/ssh/sshd_config` (e.g. `.ssh/authorized_principals`)

Now, the fun part. When `ExposeAuthInfo` is set to `yes` in `/etc/ssh/sshd_config`, the SSH server puts extra information in the environment when it's calling PAM; this info is removed before the shell starts.

The `SSH_AUTH_INFO_0` environment variable contains the SSH public key used for authentication.

When a regular unsigned RSA SSH key is used, the value looks like:

```
SSH_AUTH_INFO_0=publickey ssh-rsa AAAAB3N...
```

but when a signed RSA SSH key is used, it looks like:

```
SSH_AUTH_INFO_0=publickey ssh-rsa-cert-v01@openssh.com AAAAHHN...
```

Since SSH will only accept unexpired, signed certs from trusted CAs, it's easy to check that a signed cert was used:

```
#! /bin/bash

# fail if interrupted by a signal

exit_fail () {
  exit -1
}

trap exit_fail USR1 USR2 HUP INT ABRT TERM

ret=-1
msg="ACCEPTING Signed"

case "${SSH_AUTH_INFO_0}" in
  *ssh-rsa-cert-v01@openssh.com*)
    ret=0
    ;;
  *rsa-sha2-256-cert-v01@openssh.com*)
    ret=0
    ;;
  *rsa-sha2-512-cert-v01@openssh.com*)
    ret=0
    ;;
  *ssh-ed25519-cert-v01@openssh.com*)
    ret=0
    ;;
  *)
    msg="REJECTING Unsigned"
    ret=-1
    ;;

esac

date "+%Y%m%dT%T%z ${PAM_USER} ${msg} SSH Certificate ${SSH_AUTH_INFO_0}"

exit ${ret}

```

This script can then be used with `pam_exec` to skip `pam_duo` if the certificate was signed, by changing `/etc/pam.d/sshd` to have:

```
auth  [success=done default=ignore]  pam_exec.so quiet /usr/local/bin/pam_exec_test
auth  required                       pam_duo.so conf=/etc/security/pam_duo.conf
...
```

## SSH Agent

To ensure that signed certificates are preferred over unsigned certificates, use the `-C` option to `ssh-add`:

```
% ssh-add -D
All identities removed.
% ssh-add -C ~/.ssh/id_rsa
Could not add identity "/Users/swl/.ssh/id_rsa": success
Certificate added: /Users/swl/.ssh/id_rsa-cert.pub (animal)
% ssh-add ~/.ssh/id_rsa
Identity added: /Users/swl/.ssh/id_rsa (/Users/swl/.ssh/id_rsa)
Certificate added: /Users/swl/.ssh/id_rsa-cert.pub (animal)
% ssh-add -l
4096 SHA256:p85QXAS+amqJ8ymxdK3AVIzKrDx/7xpT4Qpw6wK+Ucc /Users/swl/.ssh/id_rsa (RSA-CERT)
4096 SHA256:p85QXAS+amqJ8ymxdK3AVIzKrDx/7xpT4Qpw6wK+Ucc /Users/swl/.ssh/id_rsa (RSA)
```

If `-C` is not used, this happens:

```
% ssh-add -D
All identities removed.
% ssh-add ~/.ssh/id_rsa
Identity added: /Users/swl/.ssh/id_rsa (/Users/swl/.ssh/id_rsa)
Certificate added: /Users/swl/.ssh/id_rsa-cert.pub (animal)
% ssh-add -l
4096 SHA256:p85QXAS+amqJ8ymxdK3AVIzKrDx/7xpT4Qpw6wK+Ucc /Users/swl/.ssh/id_rsa (RSA)
4096 SHA256:p85QXAS+amqJ8ymxdK3AVIzKrDx/7xpT4Qpw6wK+Ucc /Users/swl/.ssh/id_rsa (RSA-CERT)
```

## Authorized Key Signed by a Trusted CA

The server will accept a certificate signed by a trusted CA, and the `pam_exec` script will skip Duo authentication:

```
% SSH_AUTH_SOCK= ssh -v -i ~/.ssh/id_rsa-cert1.pub ermine.stanford.edu
debug1: Authentications that can continue: publickey,keyboard-interactive
debug1: Next authentication method: publickey
debug1: Will attempt key: /Users/swl/.ssh/id_rsa-cert1.pub RSA-CERT SHA256:p85QXAS+amqJ8ymxdK3AVIzKrDx/7xpT4Qpw6wK+Ucc explicit
debug1: Will attempt key: /Users/swl/.ssh/id_rsa RSA SHA256:p85QXAS+amqJ8ymxdK3AVIzKrDx/7xpT4Qpw6wK+Ucc explicit
debug1: Offering public key: /Users/swl/.ssh/id_rsa-cert1.pub RSA-CERT SHA256:p85QXAS+amqJ8ymxdK3AVIzKrDx/7xpT4Qpw6wK+Ucc explicit
debug1: Server accepts key: /Users/swl/.ssh/id_rsa-cert1.pub RSA-CERT SHA256:p85QXAS+amqJ8ymxdK3AVIzKrDx/7xpT4Qpw6wK+Ucc explicit
Authenticated using "publickey" with partial success.
debug1: Authentications that can continue: keyboard-interactive
debug1: Next authentication method: keyboard-interactive
Authenticated to ermine.stanford.edu ([204.63.231.2]:22) using "keyboard-interactive".
debug1: Entering interactive session.
...
Last login: ...
ermine%
```

## Authorized Key Signed by an Untrusted CA

If the server does not trust the CA used to sign the SSH certificate it will reject the
certificate; the client will retry with any other keys. If one of the alternative keys is
authorized for access to the remote account, `pam_exec` will force a Duo authentication:

```
% SSH_AUTH_SOCK= ssh -v -i ~/.ssh/id_rsa-cert2.pub ermine.stanford.edu
...
debug1: Authentications that can continue: publickey,keyboard-interactive
debug1: Next authentication method: publickey
debug1: Will attempt key: /Users/swl/.ssh/id_rsa-cert2.pub RSA-CERT SHA256:p85QXAS+amqJ8ymxdK3AVIzKrDx/7xpT4Qpw6wK+Ucc explicit
debug1: Will attempt key: /Users/swl/.ssh/id_rsa RSA SHA256:p85QXAS+amqJ8ymxdK3AVIzKrDx/7xpT4Qpw6wK+Ucc explicit
debug1: Offering public key: /Users/swl/.ssh/id_rsa-cert2.pub RSA-CERT SHA256:p85QXAS+amqJ8ymxdK3AVIzKrDx/7xpT4Qpw6wK+Ucc explicit
debug1: Authentications that can continue: publickey,keyboard-interactive
debug1: Offering public key: /Users/swl/.ssh/id_rsa RSA SHA256:p85QXAS+amqJ8ymxdK3AVIzKrDx/7xpT4Qpw6wK+Ucc explicit
debug1: Server accepts key: /Users/swl/.ssh/id_rsa RSA SHA256:p85QXAS+amqJ8ymxdK3AVIzKrDx/7xpT4Qpw6wK+Ucc explicit
Authenticated using "publickey" with partial success.
debug1: Authentications that can continue: keyboard-interactive
debug1: Next authentication method: keyboard-interactive
(swl@ermine.stanford.edu) Duo two-factor login for swl

Enter a passcode or select one of the following options:
...
```

## Authorized Key with Expired Certificate Signed by a Trusted CA

The server rejects expired signed certificates, so the client will retry with any other
keys. If one of the alternative keys is authorized for access to the remote account,
`pam_exec` will force a Duo authentication:

```
% SSH_AUTH_SOCK= ssh -v -i ~/.ssh/id_rsa-expired.pub ermine.stanford.edu
debug1: Authentications that can continue: publickey,keyboard-interactive
debug1: Next authentication method: publickey
debug1: Will attempt key: /Users/swl/.ssh/id_rsa-expired.pub RSA-CERT SHA256:p85QXAS+amqJ8ymxdK3AVIzKrDx/7xpT4Qpw6wK+Ucc explicit
debug1: Will attempt key: /Users/swl/.ssh/id_rsa RSA SHA256:p85QXAS+amqJ8ymxdK3AVIzKrDx/7xpT4Qpw6wK+Ucc explicit
debug1: Offering public key: /Users/swl/.ssh/id_rsa-expired.pub RSA-CERT SHA256:p85QXAS+amqJ8ymxdK3AVIzKrDx/7xpT4Qpw6wK+Ucc explicit
debug1: Authentications that can continue: publickey,keyboard-interactive
debug1: Offering public key: /Users/swl/.ssh/id_rsa RSA SHA256:p85QXAS+amqJ8ymxdK3AVIzKrDx/7xpT4Qpw6wK+Ucc explicit
debug1: Server accepts key: /Users/swl/.ssh/id_rsa RSA SHA256:p85QXAS+amqJ8ymxdK3AVIzKrDx/7xpT4Qpw6wK+Ucc explicit
Authenticated using "publickey" with partial success.
debug1: Authentications that can continue: keyboard-interactive
debug1: Next authentication method: keyboard-interactive
(swl@ermine.stanford.edu) Duo two-factor login for swl

Enter a passcode or select one of the following options:
```
